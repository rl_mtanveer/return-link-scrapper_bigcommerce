#  **Returns Link Scraper**
### Status Codes

Status codes indicate the current stage of scraping a retailer is in, 
this is helpful for keeping track of successfully completed retailers, or retailers
that have run into an error:

You can keep update the status of a Retailer object via:

```python
Retailer.responseStatus = 01
```

#### Table of Status Codes:
| Status Number  | Description                       |
| -----------    | -------------------------         |
| 00             | Not Started                       |
| 01             | Found Link on  HP                 |
| 02             | Link Not Found                    |
| 03             | Found FAQ Link                    |  
| 04             | FAQ Has Returns                   |
| 05             | Scraped Failed                    |   
| 06             | Pre-Scrape Competitor Indentified |
| 07             | Competitor Scrape Complete        |
| 08             | Competitor Scrape Failed          |
