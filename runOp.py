import asyncio
import copy
import random

import retailer

import aiohttp
import time
import scrape
from credentials import *
from psycopg2 import connect

# Establish Connection to Postgres Database
try:
    dbConnection = connect(host=dbHost, dbname=dbName, user=dbUser, password=dbPass)
    print(f"\n-----[Successful Connection to Database {dbName} @ {dbHost}]------")
except:
    print("Connection to the database could not be established")

# Define a Cursor Object to Execute SQL queries from the database...
cursor = dbConnection.cursor()

# Execute Query: This query gets the domains from the database where Return Url is unknown

cursor.execute(
    "SELECT domain, run_status, has_returns_home, scraped_returns_link from return_link_scrape  WHERE response_status = '500' ")

# 'Fetch' the result of the query into an 'List'----> This columns of the list can accessed like this:
# row[0] is the first column, row[1] is the second column

dbOutput = cursor.fetchall()

# List for storing 'retailer' objects, we will be creating a new object to manage each row
retailers = []

# Create an array of objects
for row in dbOutput:
    # retailers.append(retailer(row[0]))]
    # print(row[0])
    domain_url = 'http://' + row[0]
    #  print(domain_url)
    retailers.append(retailer.Retailer(row[0], domain_url, row[1], row[2], row[3]))


async def main():
    start_time = time.time()  # Start Timer

    # Set Limit for Number of Concurrent Requests
    limit = 50
    sem = asyncio.Semaphore(limit)

    # -------[ Taking all the queried domains and then turning them into tasks that will be executed concurrently
    # -------- with respect to the set Semaphore limit ----------------------------------------------------------



    async with aiohttp.ClientSession() as session:  # All Requests use the same session
        tasks = []  # Create a List of Tasks to be executed in the loop
        # Loop over each each domain, create a task to perform operations
        for retailerObject in retailers:
            tasks.append(manage_operation(retailerObject, session, sem))
        await asyncio.gather(*tasks)  # Get all tasks and put them into the 'event loop' to run

    print('\n')
    print("--- %s seconds ---" % (time.time() - start_time))  # Stop Timer


async def fetch(payload, session, sem):

    await asyncio.sleep(random.randint(1,32))

    try:
        async with sem, session.get('http://api.scraperapi.com', params=payload) as resp:
            return await resp.text(), resp.status
    except aiohttp.ClientConnectionError:
        print('Not looking good here, boss')
        return 'Failed', 500

    except aiohttp.ClientError:
        print('Not good here either..')
        return 'Failed', 500
    except UnicodeDecodeError:
        return 'Failed', 500



# -------[ This is the actual function that will run when a 'task' is executed']-----------
# Remember to refactor this name later

async def manage_operation(retailerObject, session, sem):
    await asyncio.sleep(random.randint(1,32))
    try:
        payload = {'api_key': scraper_API, 'url': retailerObject.domain_url}  # Create parameters for the URL
        fetchResponse = await fetch(payload, session, sem)  # Invoke fetch function, which return html

        retailerObject.response = fetchResponse[0]
        retailerObject.responseStatus = fetchResponse[1]

        if retailerObject.responseStatus != 200:
            retailerObject.run_status = '05'
            # retailerObject.display()
            await retailerObject.write_data(dbConnection, cursor)
        else:    
            retailerObject = copy.deepcopy(await scrape.run_home_scrape(retailerObject))

        if retailerObject.has_returns_home:
         # - [ Used for Testing Purposes]----
            await retailerObject.write_data(dbConnection, cursor)
        else:
            retailerObject = copy.deepcopy(await scrape.check_faq(retailerObject))
            # print(f"Has FAQ Link: {retailerObject.has_faq}\n FAQ Link: {retailerObject.faq_link}")

            # If a company has a FAQ link, send another request and scrape the FAQ page for returns material
            if retailerObject.responseStatus != 200:
                retailerObject.run_status = '05'

            if retailerObject.has_faq:
                payload = {'api_key': scraper_API, 'url': retailerObject.faq_link}  # Create parameters for the URL
                fetchResponse = await fetch(payload, session, sem)  # Invoke fetch function, which return html

                retailerObject.response = fetchResponse[0]
                retailerObject.responseStatus = fetchResponse[1]

                await scrape.run_scrape_faq(retailerObject)
                await retailerObject.write_data(dbConnection, cursor)

            else:
                await retailerObject.write_data(dbConnection, cursor)

    except():
        print('Oops, an error occurred')
        session = aiohttp.ClientSession(headers={"Connection": "close"})


# Beam me up, scotty!


asyncio.run(main())
