from bs4 import BeautifulSoup
import re


# ------------[Find Return Link on Homepage and Update Status]-------------
async def run_home_scrape(retailer):

    # Create a BeautifulSoup object and get all links on the page
    soup = BeautifulSoup(retailer.response, features='html.parser')
    links = soup.find_all('a')
    links = filter(lambda x: x.has_attr('href'), links)

    # List of the common anchors for return link, we will parse through to find matches

    commonAnchors = ['Returns', 'Return', 'Refund', 'Refunds', 'Return Policy', 'Return Center', 'Returns Policy',
                     'Returns Center', 'Exchanges', 'Returns and Exchanges', 'Exchange Center', 'Exchange Policy',
                     'Returns and Shipping', 'Shipping and Returns', 'Refund Policy', 'Refunds + Exchanges',
                     'Returns + Exchanges', 'Exchanges & Returns', 'Returns & Exchanges', 'Shipping & Return Policy',
                     'Return Policy & Shipping', 'Shipping & Returns', 'Shipping + Returns', 'Shipping & Exchanges',
                     'Warranty Information', 'Warranty']

    # Check each link in the returned link to see if pertains to returns or exchanges

    for link in links:
        foundTarget = False  # Used for ending the nested loop
        for anchor in commonAnchors:
            if anchor.lower() in link.text.lower():
                # print(link.attrs['href'])
                retailer.has_returns_home = True  # Indicate a HP return link exists
                retailer.scraped_returns_link = link.attrs['href']  # Set the scraped link to the object
                foundTarget = True
                break
        # If a target link is found, end this whole loop:
        if foundTarget:
            # print('Breaking Flow Early...')
            break

    # If return link found on homepage --> update the status and assign the link and return to operation manager

    if retailer.has_returns_home:
        retailer.run_status = '01'
        if retailer.scraped_returns_link.startswith('/'):
            retailer.scraped_returns_link = retailer.domain_url + retailer.scraped_returns_link
            # retailer.display()
        return retailer

    # If return not link found on homepage --> update the status w/ status code and return to operation manager
    else:
        retailer.has_returns_home = False
        retailer.run_status = '02'
        return retailer


async def check_faq(retailer):
    soup = BeautifulSoup(retailer.response, features='html.parser')
    links = soup.find_all('a')
    links = filter(lambda x: x.has_attr('href'), links)

    commonAnchors = ['Frequently Asked Questions', 'FAQs', 'FAQ', 'Frequently Asked Question',
                     'faqs', 'faq', 'FAQs and Policies', 'Shipping + faqs', 'Faqs', 'Customer Care', 'Help Center',
                     'Common Questions', 'Customer Service']
    for link in links:
        foundTarget = False  # Used for ending the nested loop
        for anchor in commonAnchors:
            if anchor.lower() in link.text.lower():
                # print(link.attrs['href'])
                retailer.has_faq = True
                retailer.faq_link = link.attrs['href']  # Set the scraped link to the object
                foundTarget = True
                break
        # If a target link is found, end this whole loop:
        if foundTarget:
            # print('Breaking Flow Early...')
            break
    if retailer.has_faq:
        if retailer.faq_link.startswith('/'):
            retailer.faq_link = retailer.domain_url + retailer.faq_link
            retailer.run_status = '03'
        retailer.display()
        return retailer
    else:
        retailer.has_faq = False;
        retailer.display()
        return retailer


async def run_scrape_faq(retailer):
    soup = BeautifulSoup(retailer.response, features='html.parser')

    commonAnchors = ['Returns', 'Return', 'Refund', 'Refunds', 'Return Policy', 'Return Center', 'Returns Policy',
                     'Returns Center', 'Exchanges', 'Returns and Exchanges', 'Exchange Center', 'Exchange Policy',
                     'Returns and Shipping', 'Shipping and Returns', 'Refund Policy', 'Refunds + Exchanges',
                     'Returns + Exchanges', 'Exchanges & Returns', 'Returns & Exchanges', 'Shipping & Return Policy',
                     'Return Policy & Shipping', 'Shipping & Returns', 'Shipping + Returns', 'Shipping & Exchanges',
                     'Warranty Information', 'Warranty', 'Start a Return', 'Click here to return', 'Begin a Return']

    for anchor in commonAnchors:
        foundTarget = False  # Used for ending the nested loop
        for elem in soup(text=re.compile(anchor)):
            # print(elem)
            foundTarget = True
            break
        if foundTarget:
            retailer.faq_has_returns = True;
            break

    if retailer.faq_has_returns:
        retailer.scraped_returns_link = retailer.faq_link
        retailer.run_status = '04'
        retailer.display()
        return retailer
    else:
        retailer.run_status = '02'
        retailer.display()
        return retailer
