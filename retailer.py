# This class will house the information for each domain

class Retailer:
    # 'Constructor' maps to the query from the database, and instantiates a new object
    def __init__(self, domain, domain_url, run_status, has_returns_home, scraped_returns_link):

        self.domain = domain
        self.domain_url = domain_url
        self.run_status = run_status
        self.has_returns_home = has_returns_home
        self.scraped_returns_link = scraped_returns_link

        self.response = 'No Response Recorded'
        self.responseStatus = 00

        self.has_faq = False
        self.faq_link = 'N/A'
        self.faq_response = 'No Response Recorded'
        self.faq_has_returns = False

        self.csLink = 'N/A'

    # -- Used for troubleshooting and verification purposes:

    def display(self):
        print(
            f"\nDomain: {self.domain_url}  Response Status: {self.responseStatus}\nRun Status: {self.run_status} \nReturns "
            f"Link on Homepage: {self.has_returns_home}  Scraped URL: {self.scraped_returns_link}")

    # -- Write collected retailer data to database, this should be agnostic to the status of the scrape

    async def write_data(self, connection, cursor):
        # print('Writing data...')
        try:
            sql  = """ UPDATE return_link_scrape
                SET has_returns_home = %s, run_status = %s, scraped_returns_link = %s, response = %s, 
                response_status = %s
                WHERE domain = %s"""

            cursor.execute(sql,(self.has_returns_home, self.run_status, self.scraped_returns_link, self.response,
                             self.responseStatus,self.domain))
            connection.commit()
        except:
            print('Sad!')



